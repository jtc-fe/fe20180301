// number
var songuyen = 15;
var sothapphan = 15.56;

// string
var chuoi1 = ""
var chuoi2 = ''

// boolean
var sai = false

// Bai 1
var sothu1 = 4;
var sothu2 = 6;

// so 4 + so 5 = 9
var kq = sothu1 + sothu2;
var chuoiKq1 = "so " + sothu1 + " + so " + sothu2 + " = " + kq;
console.log(chuoiKq1);

var chuoiKq2 = `so ${sothu1} + so ${sothu2} = ${kq}`;
console.log(chuoiKq2);

// Giai phuong trinh bac 2
var a = 4;
var b = 2;
var c = -6;

var n1, n2;

// luon luon a > 0

var delta = b * b - 4 * a * c;

if (delta < 0) {
    console.log("Phuong trinh vo nghiem");
}
else if (delta == 0) {
    n1 = n2 = -b / (2 * a);
    console.log("Phuong trinh co 1 nghiem = " + n1);
} else {
    n1 = (-b - Math.sqrt(delta)) / (2 * a);
    n2 = (-b + Math.sqrt(delta)) / (2 * a);
    console.log("Phuong trinh co 2 nghiem");
    console.log("nghiem 1 = " + n1);
    console.log("nghiem 2 = " + n2);
}


var sothu1 = 3;
var sothu2 = 4;

console.log("ket qua la " + sothu1 + sothu2);
console.log(sothu1 + sothu2);


// Pham vi cua bien

var bienCuaToi = 5;  // global

myFunc();

console.log(bienCuaToi);


function myFunc() {
    var bienCuaToi = 10;  // local
    console.log(bienCuaToi) // 10
}


if (true) {
    console.log('ahihi dung roi')
} else {
    console.log('sai roi nhe')
}

(a && b) ? console.log('ahihi dung roi') : console.log('sai roi');


// toan tu typeof
var so = 5;
var chuoi = "ahihi";
var dung = true;
var khongkhaibao;
var ham = function () {

};
var doituong = {};
var kieuNull = null;

console.log('------');
console.log(typeof (so));
console.log(typeof (chuoi));
console.log(typeof (dung));
console.log(typeof (khongkhaibao));
console.log(typeof (ham));
console.log(typeof (doituong));
console.log(typeof (kieuNull));


var tenmon = "sting";

switch (tenmon) {
    case 'coca':
        console.log('8k');
        break;
    case 'sting':
        console.log('10k');
        break;
    case 'aqua':
        console.log('5k');
        break;
    default:
        console.log("mon may chon khong co dau");
}


var abc = 5;
console.log(typeof(so)); // number

abc = "chuoi cua tao";
console.log(typeof(so)); // string


// In chuoi cac so tu 1 - 10 voi vong lap While
console.log("In chuoi cac so tu 1 - 10 voi vong lap While")

var i = 1;
while (i <= 10) {
    // code
    console.log(i);
    i++;
}
console.log('-----');
var i = 1;
while (true) {
    console.log(i);
    i++;
    if (i > 10) break;
}


// In chuoi cac so tu 1 - 10 voi vong lap Do - While
console.log("In chuoi cac so tu 1 - 10 voi vong lap Do - While")

// hai
var j = 1;
do {
    console.log(j);  // 1, 2, 3, ..., 9, 10
    j++; // 2, 3, 4, ... 10, 11
} while (j <= 10);


// Vong lap for
var array = ["chuoi", "dua hau", "le", "tao"];

for (var i = 0; i < array.length; i++) {
    console.log(array[i]);
    // "chuoi", "dua hau", "le", "tao"
}

var chuoi = "xin chao cac ban";
var chuoiKq = "";
// "XIN CHAO CAC BAN"
for (var i = 0; i < chuoi.length; i++) {
    chuoiKq += chuoi[i].toUpperCase();
}
console.log(chuoiKq);

// Tinh tong S(n) = 1 + 2 + 3 + ... + n
var n = 100;

var tong = 0;
var i = 1;
while (i <= 100) {
    tong = tong + i;
    i++;
}
i == 101

i = 1;
console.log(tong);

// Tim gia tri lon nhat cua 4 so a,b,c,d

// for
var a = 4, b = 5, c = 3, d = 1;
var arrayInput = [a,b,c,d];
var max = arrayInput[0]; // max = 4

for (var i = 0; i< arrayInput.length; i++) {
    // i = 0, 1, 2
    if (arrayInput[i] > arrayInput[i + 1]) {
        max = arrayInput[i]
    }
    // max = 3
}

console.log(max);


// if else
var a = 4, b = 3, c = 5, d = 1;
var arrayInput = [a,b,c,d, 6, 9 , 10, 4, 2, 8];
var max = a;

for(var i=0; i<arrayInput.length; i++) {
    if (arrayInput[i] > max) {
        max = arrayInput[i];
    }
}

console.log(max)

var a = 4, b = 3, c = 5, d = 1;
var max = a;

if (b > max) max = b;
if (c > max) max = c;
if (d > max) max = d;

max = 5;


// In ra bang cuu chuong
for(var i = 1; i <= 10; i++) {
    console.log("Chuong " + i);
    for (var j = 1; j <= 10; j++) {
        var m = i * j;
        console.log(i + " * " + j + " = " + m);
    }
}

// dao nguoc chuoi - abcde -> edcba
var chuoi = "abcdetfg";
var chuoiKq = "";

for (var i = chuoi.length - 1; i >= 0; i--) {
    chuoiKq += chuoi[i];
}

console.log(chuoiKq);


// Ham
function loaiBoKyTuTrung(input) {
    var output = "";

    for(var i=0; i<input.length; i++) {
        // kiem tra input[i] co trong output hay chua
        if (kiemTraKyTuTrongChuoi(input[i], output) == false) {
            output += input[i];
        }
    }

    return output;
}

function kiemTraKyTuTrongChuoi(kytu, chuoi) {
    for(var i=0; i<chuoi.length; i++) {
        if (kytu == chuoi[i]) {
            return true
        }
    }
    return false;
}

var ketqua = loaiBoKyTuTrung("thequickbrownfoxjumpsoverthelazydog");
console.log(ketqua);


function kiemTraSoNguyenTo(so1) {
    if (so1 < 2) return false;

    for (var i=2; i < so1; i++) {
        if (so1 % i == 0) return false;
    }

    return true;
}

kiemTraSoNguyenTo(15);

function vietHoaChuCaiDau(chuoiInput) {
    var chuoiKq = "";

    for(var i=0; i<chuoiInput.length; i++) {
        if (i == 0 || chuoiInput[i - 1] == " ") {
            chuoiKq += chuoiInput[i].toUpperCase()
        } else {
            chuoiKq += chuoiInput[i];
        }
    }

    console.log(chuoiKq);
}

vietHoaChuCaiDau("day la bong hoa nho");


// write jquery code
$(document).ready(function() {


    $("div > h1").css({"background-color": "red"});
    
    $("form input").hide()

    $("#myselect").change(function() {
        console.log($("#myselect").val())
    })

    var list = $('ul li');

    for(var i=0; i<list.length; i++) {
        $('ul li').eq(i).prepend(`<b>${i}:</b>`)
    }

    $("p").addClass("myclass")

    $('p').click(function() {
        console.log("click event");
        alert("click");
    })

    $('p').dblclick(function() {
        console.log('double click event');
    })

    $("#show-value-btn").click(function() {
        var input = $("#input-text").val();
        
        console.log(input);
    })

    var dem = 0;
    $("#change-color").click(function() {
    //     $("#parent > p").css(
    //         {"color": "red",
    //     "background-color": "blue",
    // "font-size": "35px"}
    //     );

        // console.log($("#list-paragraph p").eq(0))

        $("#list-paragraph p").css({"color": "black"});
        $("#list-paragraph p").eq(dem).css({"color": "red"});
        dem++;
        if (dem == $("#list-paragraph p").length) {
            dem = 0;
        }
        


    })

    var scopeListCar = [];
    loadJSONData()

    $("#input-text").keyup(function(e) {
        if(e.keyCode == 13) {
            search()
        }
    })

$("#localStorage").click(function() {

    var giohang = {
        "order_id": "fjldgjsdgk",
        "list_item": [
            {
                "id": "iphonex",
                "name": "iphone X",
                "price": 100000,
                "quality": 4
            },
            {
                "id": "sss9",
                "name": "samsung S9",
                "price": 200000,
                "quality": 2
            }
        ]
    }


    window.localStorage.setItem("giohang", JSON.stringify(giohang));
})

$("#getLocalStorage").click(function() {

    var giohangLocalstorage = window.localStorage.getItem("giohang");
    console.log(giohangLocalstorage)
    var giohang = JSON.parse(giohangLocalstorage);
    console.log(giohang)
    

    var item = {
        id: "nokia",
        name: "nokia",
        price: 567777,
        quality: 5
    }
    giohang.list_item.push(item);

    window.localStorage.setItem("giohang", JSON.stringify(giohang))

    window.localStorage.removeItem("giohang");
})



    function search() {
        var searchText = $("#input-text").val().toLowerCase();

        window.location.href = "search.html?searchText=" + searchText;


        // $("#list_car").empty();

        // for(var i=0; i<scopeListCar.length; i++) {
        //     var car = scopeListCar[i];

        //     if (car.name.toLowerCase().indexOf(searchText) != -1 ||
        //     car.description.toLowerCase().indexOf(searchText) != -1) {
        //         var itemHtml = '<div class="each-news col-12 col-sm-6 col-md-4 col-lg-4">'
        //             itemHtml += `<a href="detail.html?car_id=${car.id}"><img src="${car.img_url}">`;
        //             itemHtml += `<p class="title">${car.name}</p>`;
        //             itemHtml += `<p class="author">${car.description}</p></a>`;
        //             itemHtml += '</div>'
    
                    
        //             $("#list_car").append(itemHtml);
        //     }
        // }
    }

    $("#search-btn").click(function() {
        search()
    })

    function loadJSONData() {
        $.ajax({
            url: "https://www.jasonbase.com/things/7qol.json",
            success: function(result) {

                var listCar = result.list_car;

                scopeListCar = listCar

                for(var i=0; i<listCar.length; i++) {

                    var car = listCar[i];

                    console.log(car)

                    var itemHtml = '<div class="each-news col-12 col-sm-6 col-md-4 col-lg-4">'
                    itemHtml += `<a href="detail.html?car_id=${car.id}"><img src="${car.img_url}">`;
                    itemHtml += `<p class="title">${car.name}</p>`;
                    itemHtml += `<p class="author">${car.description}</p></a>`;
                    itemHtml += '</div>'
    
                    
                    $("#list_car").append(itemHtml);
                }

                
                    
                    
                
            }
        })
    }


})