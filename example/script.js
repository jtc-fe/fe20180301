// number
var songuyen = 15;
var sothapphan = 15.56;

// string
var chuoi1 = ""
var chuoi2 = ''

// boolean
var sai = false

// Bai 1
var sothu1 = 4;
var sothu2 = 6;

// so 4 + so 5 = 9
var kq = sothu1 + sothu2;
var chuoiKq1 = "so " + sothu1 + " + so " + sothu2 + " = " + kq;
console.log(chuoiKq1);

var chuoiKq2 = `so ${sothu1} + so ${sothu2} = ${kq}`;
console.log(chuoiKq2);

// Giai phuong trinh bac 2
var a = 4;
var b = 2;
var c = -6;

var n1, n2;

// luon luon a > 0

var delta = b * b - 4 * a * c;

if (delta < 0) {
    console.log("Phuong trinh vo nghiem");
}
else if (delta == 0) {
    n1 = n2 = -b / (2 * a);
    console.log("Phuong trinh co 1 nghiem = " + n1);
} else {
    n1 = (-b - Math.sqrt(delta)) / (2 * a);
    n2 = (-b + Math.sqrt(delta)) / (2 * a);
    console.log("Phuong trinh co 2 nghiem");
    console.log("nghiem 1 = " + n1);
    console.log("nghiem 2 = " + n2);
}


var sothu1 = 3;
var sothu2 = 4;

console.log("ket qua la " + sothu1 + sothu2);
console.log(sothu1 + sothu2);


// Pham vi cua bien

var bienCuaToi = 5;  // global

myFunc();

console.log(bienCuaToi);


function myFunc() {
    var bienCuaToi = 10;  // local
    console.log(bienCuaToi) // 10
}


if (true) {
    console.log('ahihi dung roi')
} else {
    console.log('sai roi nhe')
}

(a && b) ? console.log('ahihi dung roi') : console.log('sai roi');


// toan tu typeof
var so = 5;
var chuoi = "ahihi";
var dung = true;
var khongkhaibao;
var ham = function () {

};
var doituong = {};
var kieuNull = null;

console.log('------');
console.log(typeof (so));
console.log(typeof (chuoi));
console.log(typeof (dung));
console.log(typeof (khongkhaibao));
console.log(typeof (ham));
console.log(typeof (doituong));
console.log(typeof (kieuNull));


var tenmon = "sting";

switch (tenmon) {
    case 'coca':
        console.log('8k');
        break;
    case 'sting':
        console.log('10k');
        break;
    case 'aqua':
        console.log('5k');
        break;
    default:
        console.log("mon may chon khong co dau");
}


var abc = 5;
console.log(typeof(so)); // number

abc = "chuoi cua tao";
console.log(typeof(so)); // string


// In chuoi cac so tu 1 - 10 voi vong lap While
console.log("In chuoi cac so tu 1 - 10 voi vong lap While")

var i = 1;
while (i <= 10) {
    // code
    console.log(i);
    i++;
}
console.log('-----');
var i = 1;
while (true) {
    console.log(i);
    i++;
    if (i > 10) break;
}


// In chuoi cac so tu 1 - 10 voi vong lap Do - While
console.log("In chuoi cac so tu 1 - 10 voi vong lap Do - While")

// hai
var j = 1;
do {
    console.log(j);  // 1, 2, 3, ..., 9, 10
    j++; // 2, 3, 4, ... 10, 11
} while (j <= 10);


// Vong lap for
var array = ["chuoi", "dua hau", "le", "tao"];

for (var i = 0; i < array.length; i++) {
    console.log(array[i]);
    // "chuoi", "dua hau", "le", "tao"
}

var chuoi = "xin chao cac ban";
var chuoiKq = "";
// "XIN CHAO CAC BAN"
for (var i = 0; i < chuoi.length; i++) {
    chuoiKq += chuoi[i].toUpperCase();
}
console.log(chuoiKq);

// Tinh tong S(n) = 1 + 2 + 3 + ... + n
var n = 100;

var tong = 0;
var i = 1;
while (i <= 100) {
    tong = tong + i;
    i++;
}
i == 101

i = 1;
console.log(tong);

// Tim gia tri lon nhat cua 4 so a,b,c,d

// for
var a = 4, b = 5, c = 3, d = 1;
var arrayInput = [a,b,c,d];
var max = arrayInput[0]; // max = 4

for (var i = 0; i< arrayInput.length; i++) {
    // i = 0, 1, 2
    if (arrayInput[i] > arrayInput[i + 1]) {
        max = arrayInput[i]
    }
    // max = 3
}

console.log(max);


// if else
var a = 4, b = 3, c = 5, d = 1;
var arrayInput = [a,b,c,d, 6, 9 , 10, 4, 2, 8];
var max = a;

for(var i=0; i<arrayInput.length; i++) {
    if (arrayInput[i] > max) {
        max = arrayInput[i];
    }
}

console.log(max)

var a = 4, b = 3, c = 5, d = 1;
var max = a;

if (b > max) max = b;
if (c > max) max = c;
if (d > max) max = d;

max = 5;


// In ra bang cuu chuong
for(var i = 1; i <= 10; i++) {
    console.log("Chuong " + i);
    for (var j = 1; j <= 10; j++) {
        var m = i * j;
        console.log(i + " * " + j + " = " + m);
    }
}

// dao nguoc chuoi - abcde -> edcba
var chuoi = "abcdetfg";
var chuoiKq = "";

for (var i = chuoi.length - 1; i >= 0; i--) {
    chuoiKq += chuoi[i];
}

console.log(chuoiKq);


// Ham
function loaiBoKyTuTrung(input) {
    var output = "";

    for(var i=0; i<input.length; i++) {
        // kiem tra input[i] co trong output hay chua
        if (kiemTraKyTuTrongChuoi(input[i], output) == false) {
            output += input[i];
        }
    }

    return output;
}

function kiemTraKyTuTrongChuoi(kytu, chuoi) {
    for(var i=0; i<chuoi.length; i++) {
        if (kytu == chuoi[i]) {
            return true
        }
    }
    return false;
}

var ketqua = loaiBoKyTuTrung("thequickbrownfoxjumpsoverthelazydog");
console.log(ketqua);


function kiemTraSoNguyenTo(so1) {
    if (so1 < 2) return false;

    for (var i=2; i < so1; i++) {
        if (so1 % i == 0) return false;
    }

    return true;
}

kiemTraSoNguyenTo(15);

function vietHoaChuCaiDau(chuoiInput) {
    var chuoiKq = "";

    for(var i=0; i<chuoiInput.length; i++) {
        if (i == 0 || chuoiInput[i - 1] == " ") {
            chuoiKq += chuoiInput[i].toUpperCase()
        } else {
            chuoiKq += chuoiInput[i];
        }
    }

    console.log(chuoiKq);
}

vietHoaChuCaiDau("day la bong hoa nho");


function lietKe(n) {
    console.log("liet ke so le")
    for(var i=0; i<=n; i++) {
        if (kiemTraSoNguyenTo(i) == true) {
            console.log(i);
        }
    }
}

function kiemTraSoLe(sobatky) {
    if (sobatky%2 != 0) {
        return true
    } else {
        return false
    }
}

function kiemTraSoNguyenTo(sobatky) {
    // so nguyen to la so chia het cho 1 va chinh no
    // tuc la tat ca cac so tu 2 -> no - 1 thi no khong chia het
    if (sobatky < 2) return false;
    for(var i=2; i<sobatky; i++) {
        if (sobatky % i == 0) return false;
    }
    return true;
}

lietKe(100);

function tatCaVeUocSo(so) {
    var tong = 0;
    var tich = 1;
    var dem = 0;
    console.log("Liet ke uoc so cua " + so);
    for(var i=0; i<=so; i++) {
        if (kiemTraUocSo(i, so)) {
            console.log(i);
            tong += i;
            tich *= i;
            dem++;
        }
    }
    console.log("tong = " + tong);
    console.log("tich = " + tich);
    console.log("dem = " + dem);
}

function kiemTraUocSo(i, n) {
    if (n%i == 0) {
        return true;
    } else {
        return false;
    }
}

tatCaVeUocSo(10);

var n = 3535;
var dem = 0;
while(n > 0) {
    n = parseInt(n / 10);
    dem++;
}
console.log("so cac chu so la: " + dem);



function tinhTong(n) {
    var tong = 0;
    for(var i=1; i<=n; i++) {
        tong += i;
    }
    return tong;
}

function timSoNguyenDuongN() {
    var i = 0;
    while(true) {
        if (tinhTong(i) > 10000) return i;
        i++;
    }
}

var ketqua = timSoNguyenDuongN();
console.log(ketqua)


function showEmail() {
    var emailInput = document.getElementById("email");
    var email = emailInput.value;
    console.log(email);
}

function changeColorBlue() {
    var emailInput = document.getElementById("email");
    // emailInput.style.color = "red";
    // emailInput.style.backgroundColor = "green";
    // emailInput.style.fontSize = "35px";

    emailInput.classList.add("color-red");

    emailInput.classList.remove("color-red");
}

function js_style() {
    var elementText = document.getElementById("text");
    // change font
    elementText.style.fontFamily = "Arial";
    elementText.style.fontSize = "23px";
    elementText.style.color = "red";
}

function onSubmit() {

    console.log('12345555');

    var firstName = document.getElementById("f_name").value;
    var lastName = document.getElementById("l_name").value;
    console.log(firstName + " " + lastName);
}

function removecolor() {
    var selectEle = document.getElementById("colorSelect");
    selectEle.remove(0);
}

var arrayImage = ["http://farm4.staticflickr.com/3691/11268502654_f28f05966c_m.jpg",
"http://farm1.staticflickr.com/33/45336904_1aef569b30_n.jpg",
"http://farm6.staticflickr.com/5211/5384592886_80a512e2c9.jpg"];

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function display_random_image() {
    var imageEle = document.getElementById("random_img");

    var index = getRandomInt(0, 2);

    imageEle.src = arrayImage[index];
}